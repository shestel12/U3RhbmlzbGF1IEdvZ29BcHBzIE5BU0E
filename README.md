# Url-downloader

Test task that implements a microservice responsible for gathering image URLs from the open NASA's APOD API.

## Setup

To run the application using provided Dockerfile `make docker-run` command can be used. It will automatically get
required environment variables from the host machine (if they are set), otherwise default values for them will be used
in application

| Field               |      Type      | Required | Default  | Description                                                                                                                        |
|---------------------|:--------------:|:--------:|:--------:|--------------------------------------------------------------------------------------------------------------------------|
| API_KEY             |      string    |    no    | DEMO_KEY | API Key that will be used to query APOD API                                                           |
| CONCURRENT_REQUESTS |       int      |    no    |    5     | Maximum number of concurrent requests that can be made to the APOD API                                                                                                                 |
| PORT                |       int      |    no    |  8080    | HTTP port on which application will be launched                                                                                    | |

## Additional notes

* APOD NASA API provides has the capability to specify a range of dates using `start_date` and `end_date` request
  parameters, but since the task description mentioned only the `date` parameter and there was a notice about querying
  the api concurrently for broad date range requests I used only `date` in queries to APOD API.
* The name of the repository is base64 encoded string, but padding `=` character at the end of the string had to be removed, 
  since Gitlab does not allow it in the repository name. Full string would look like this: 
  `U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E=`
