package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/handler"
	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/service"
)

const defaultKey = "DEMO_KEY"
const defaultConcurrentRequests = 5
const defaultPort = 8080

func main() {
	apiKey := getEnvOrDefault("API_KEY", defaultKey)
	concurrentRequests, err := getEnvIntOrDefault("CONCURRENT_REQUESTS", defaultConcurrentRequests)
	if err != nil {
		log.Fatal(err)
	}
	if concurrentRequests <= 0 {
		log.Fatal("invalid CONCURRENT_REQUESTS env var, needs to be a positive number")
	}
	port, err := getEnvIntOrDefault("PORT", defaultPort)
	if err != nil {
		log.Fatal(err)
	}

	pictureService := service.NewPictureService(apiKey, concurrentRequests)
	pictureHandler := handler.NewHandler(pictureService)

	r := mux.NewRouter()

	r.HandleFunc("/pictures", pictureHandler.GetPictures)

	log.Infof("url-collector starting, listening on port :%d", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), r))
}

func getEnvOrDefault(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func getEnvIntOrDefault(key string, fallback int) (int, error) {
	strValue := os.Getenv(key)
	if len(strValue) == 0 {
		return fallback, nil
	}
	value, err := strconv.Atoi(strValue)
	if err != nil {
		return 0, fmt.Errorf("invalid %s env var, needs to be integer. err:%w", key, err)
	}
	return value, nil
}
