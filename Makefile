ifndef PORT
override PORT = 8080
endif

.PHONY: docker-run
docker-run:
	docker build -t url-collector \
	--build-arg API_KEY=${API_KEY} \
	--build-arg CONCURRENT_REQUESTS=${CONCURRENT_REQUESTS} \
	--build-arg PORT=${PORT} \
 	. && docker run -p ${PORT}:${PORT} \
	-it url-collector \

.PHONY: lint
lint:
	golangci-lint run

.PHONY: test
test:
	go test ./...

.PHONY: run
run:
	go run main.go

.PHONY: generate-mocks
generate-mocks:
	mockery --dir=service --all --output=./service/mocks
