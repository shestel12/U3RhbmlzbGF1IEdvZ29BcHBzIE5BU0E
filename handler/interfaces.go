package handler

import (
	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/domain"
)

type PictureService interface {
	GetPictures(parameters domain.PicturesParameters) (domain.Pictures, error)
}
