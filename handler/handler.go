package handler

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type Handler struct {
	service PictureService
}

func NewHandler(service PictureService) *Handler {
	return &Handler{
		service: service,
	}
}

func (h *Handler) jsonReturn(w http.ResponseWriter, statusCode int, jsonObject interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)

	err := json.NewEncoder(w).Encode(jsonObject)
	if err != nil {
		log.WithField("jsonObject", jsonObject).Warn(log.Entry{
			Message: "could not encode json return: " + err.Error(),
		})
	}
}
