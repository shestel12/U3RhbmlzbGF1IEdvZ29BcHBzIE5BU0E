package handler

import (
	"errors"
	"net/http"

	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/domain"
)

type errorResponse struct {
	Error string `json:"error"`
}

func (h *Handler) returnError(w http.ResponseWriter, err error) {
	var response errorResponse

	var status int

	response = errorResponse{Error: err.Error()}
	switch {
	case errors.Is(err, domain.ErrInvalidParameter):
		status = http.StatusBadRequest
	case errors.Is(err, domain.ErrExternalAPIError) || errors.Is(err, domain.ErrInternalServerError):
		status = http.StatusInternalServerError
	default:
		status = http.StatusInternalServerError
	}

	h.jsonReturn(w, status, response)
}
