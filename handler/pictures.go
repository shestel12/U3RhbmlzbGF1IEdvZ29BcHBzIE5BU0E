package handler

import (
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/domain"
)

const dateFormat = "2006-01-02"

type getPicturesRequest struct {
	StartDate time.Time
	EndDate   time.Time
}

type picturesResponse struct {
	URLs []string `json:"urls"`
}

func buildPicturesRequest(r *http.Request) (getPicturesRequest, error) {
	startDate, err := time.Parse(dateFormat, r.URL.Query().Get("start_date"))
	if err != nil {
		return getPicturesRequest{}, fmt.Errorf("%w: start_date GET parameter must have a '2006-01-02' format ", domain.ErrInvalidParameter)
	}

	endDate, err := time.Parse(dateFormat, r.URL.Query().Get("end_date"))
	if err != nil {
		return getPicturesRequest{}, fmt.Errorf("%w: end_date GET parameter must have a '2006-01-02' format ", domain.ErrInvalidParameter)
	}

	if endDate.Before(startDate) {
		return getPicturesRequest{}, fmt.Errorf("%w: start_date cannot be before end_date", domain.ErrInvalidParameter)
	}

	return getPicturesRequest{
		StartDate: startDate,
		EndDate:   endDate,
	}, nil
}

func (h Handler) GetPictures(w http.ResponseWriter, r *http.Request) {
	var req getPicturesRequest

	req, err := buildPicturesRequest(r)
	if err != nil {
		log.Errorf("error on building picture request: %s", err)
		h.returnError(w, err)
		return
	}

	pictures, err := h.service.GetPictures(domain.PicturesParameters{
		StartDate: req.StartDate,
		EndDate:   req.EndDate,
	})
	if err != nil {
		log.Errorf("error on getting pictures: %s", err)
		h.returnError(w, err)
		return
	}

	h.jsonReturn(w, http.StatusOK, picturesResponse{
		URLs: pictures.URLs,
	})
}
