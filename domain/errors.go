package domain

import "errors"

var (
	ErrInvalidParameter    = errors.New("invalid parameter")
	ErrExternalAPIError    = errors.New("external api error")
	ErrInternalServerError = errors.New("internal server error")
)
