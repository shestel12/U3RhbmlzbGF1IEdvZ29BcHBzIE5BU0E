package domain

import (
	"time"
)

type PicturesParameters struct {
	StartDate time.Time
	EndDate   time.Time
}

type Pictures struct {
	URLs []string
}
