package service

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/domain"
	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/service/mocks"
)

func TestService_GetPictures(t *testing.T) {
	t.Parallel()
	apiKey := "test_key"

	type fields struct {
		httpClient *mocks.HttpClient
		parameters domain.PicturesParameters
	}

	testStartTime := time.Now()
	testEndTime := time.Now().AddDate(0, 0, 1)
	testEndTimeInvalid := time.Now().AddDate(0, 0, 1001)

	url1 := "https://url.com/picture"
	examplePictureJSON := fmt.Sprintf("{\"url\":\"%s\"}", url1)
	url2 := "https://url.com/picture/2"
	examplePictureJSON2 := fmt.Sprintf("{\"url\":\"%s\"}", url2)

	tests := []struct {
		name               string
		fields             fields
		setupExpectedCalls func(*mocks.HttpClient)
		expectedResult     domain.Pictures
		expectedErr        error
	}{
		{
			name: "err: Start date cannot be before end date",
			fields: fields{httpClient: &mocks.HttpClient{}, parameters: domain.PicturesParameters{
				StartDate: testEndTime,
				EndDate:   testStartTime,
			}},
			expectedErr: domain.ErrInvalidParameter,
		},
		{
			name: "err: Date interval cannot exceed 1000 days",
			fields: fields{httpClient: &mocks.HttpClient{}, parameters: domain.PicturesParameters{
				StartDate: testStartTime,
				EndDate:   testEndTimeInvalid,
			}},
			expectedErr: domain.ErrInvalidParameter,
		},
		{
			name: "err: If there was an error on sending GET request, internal server error has to be returned",
			fields: fields{httpClient: &mocks.HttpClient{}, parameters: domain.PicturesParameters{
				StartDate: testStartTime,
				EndDate:   testStartTime,
			}},
			setupExpectedCalls: func(client *mocks.HttpClient) {
				client.On("Get", fmt.Sprintf(apodURL, apiKey, testStartTime.Format(apodDateFormat))).Return(nil, errors.New("error"))
			},
			expectedErr: domain.ErrInternalServerError,
		},
		{
			name: "err: If the status code from NASA APOD API is not 200, external API error needs to be returned",
			fields: fields{httpClient: &mocks.HttpClient{}, parameters: domain.PicturesParameters{
				StartDate: testStartTime,
				EndDate:   testStartTime,
			}},
			setupExpectedCalls: func(client *mocks.HttpClient) {
				client.On("Get", fmt.Sprintf(apodURL, apiKey, testStartTime.Format(apodDateFormat))).Return(&http.Response{
					StatusCode: 429,
					Body:       ioutil.NopCloser(bytes.NewReader([]byte("{}"))),
				}, nil)
			},
			expectedErr: domain.ErrExternalAPIError,
		},
		{
			name: "err: If there was an issue on decoding json response, internal server error needs to be returned",
			fields: fields{httpClient: &mocks.HttpClient{}, parameters: domain.PicturesParameters{
				StartDate: testStartTime,
				EndDate:   testStartTime,
			}},
			setupExpectedCalls: func(client *mocks.HttpClient) {
				client.On("Get", fmt.Sprintf(apodURL, apiKey, testStartTime.Format(apodDateFormat))).Return(&http.Response{
					StatusCode: 200,
					Body:       ioutil.NopCloser(bytes.NewReader([]byte{})),
				}, nil)
			},
			expectedErr: domain.ErrInternalServerError,
		},
		{
			name: "ok: If there was a successful request made for the same start/end date, 1 picture needs to be returned",
			fields: fields{httpClient: &mocks.HttpClient{}, parameters: domain.PicturesParameters{
				StartDate: testStartTime,
				EndDate:   testStartTime,
			}},
			setupExpectedCalls: func(client *mocks.HttpClient) {
				client.On("Get", fmt.Sprintf(apodURL, apiKey, testStartTime.Format(apodDateFormat))).Return(&http.Response{
					StatusCode: 200,
					Body:       ioutil.NopCloser(bytes.NewReader([]byte(examplePictureJSON))),
				}, nil)
			},
			expectedResult: domain.Pictures{URLs: []string{url1}},
		},
		{
			name: "ok: If there was a successful request made for the different start/end date, several pictures needs to be returned",
			fields: fields{httpClient: &mocks.HttpClient{}, parameters: domain.PicturesParameters{
				StartDate: testStartTime,
				EndDate:   testEndTime,
			}},
			setupExpectedCalls: func(client *mocks.HttpClient) {
				client.On("Get", fmt.Sprintf(apodURL, apiKey, testStartTime.Format(apodDateFormat))).Return(&http.Response{
					StatusCode: 200,
					Body:       ioutil.NopCloser(bytes.NewReader([]byte(examplePictureJSON))),
				}, nil).Once()

				client.On("Get", fmt.Sprintf(apodURL, apiKey, testEndTime.Format(apodDateFormat))).Return(&http.Response{
					StatusCode: 200,
					Body:       ioutil.NopCloser(bytes.NewReader([]byte(examplePictureJSON2))),
				}, nil).Once()
			},
			expectedResult: domain.Pictures{URLs: []string{url1, url2}},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			svc := &PictureService{
				apiKey:            apiKey,
				client:            tt.fields.httpClient,
				runningGoroutines: make(chan struct{}, 1),
			}

			if tt.setupExpectedCalls != nil {
				tt.setupExpectedCalls(tt.fields.httpClient)
			}

			req := require.New(t)
			pictures, err := svc.GetPictures(tt.fields.parameters)

			if tt.expectedErr != nil {
				req.ErrorIs(err, tt.expectedErr)
			} else {
				req.NoError(err)
				req.Equal(tt.expectedResult, pictures)
			}

			tt.fields.httpClient.AssertExpectations(t)
		})
	}
}
