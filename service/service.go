package service

import (
	"net/http"
)

type PictureService struct {
	apiKey            string
	client            HTTPClient
	runningGoroutines chan struct{}
}

func NewPictureService(apiKey string, maxGoroutines int) *PictureService {
	return &PictureService{
		apiKey: apiKey,
		client: &http.Client{
			Timeout: defaultTimeout,
		},
		runningGoroutines: make(chan struct{}, maxGoroutines),
	}
}
