package service

import (
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E/domain"
)

const apodURL = "https://api.nasa.gov/planetary/apod?api_key=%s&date=%s"
const apodDateFormat = "2006-01-02"
const defaultTimeout = time.Second * 10
const apiMaxHourlyCalls = 1000

type pictureResponse struct {
	URL string `json:"url"`
}

type errResponse struct {
	Code    int    `json:"code"`
	Message string `json:"msg"`
}

func (c *PictureService) GetPictures(parameters domain.PicturesParameters) (domain.Pictures, error) {
	hoursInDay := 24
	startDate := parameters.StartDate.Truncate(time.Hour * time.Duration(hoursInDay))
	endDate := parameters.EndDate.Truncate(time.Hour * time.Duration(hoursInDay))
	daysCount := int(math.Ceil(endDate.Sub(startDate).Hours()/float64(hoursInDay))) + 1

	if daysCount > apiMaxHourlyCalls {
		return domain.Pictures{}, fmt.Errorf("%w: date interval cannot be higher that %d days", domain.ErrInvalidParameter, apiMaxHourlyCalls)
	}

	if daysCount <= 0 {
		return domain.Pictures{}, fmt.Errorf("%w: date interval cmust include at least 1 day", domain.ErrInvalidParameter)
	}

	pictures := domain.Pictures{
		URLs: make([]string, 0, daysCount),
	}

	resChan := make(chan pictureResponse, daysCount)
	errChan := make(chan error, daysCount)
	wg := sync.WaitGroup{}

	currentDate := startDate
	for i := 0; i < daysCount; i++ {
		wg.Add(1)
		c.runningGoroutines <- struct{}{}

		url := fmt.Sprintf(apodURL, c.apiKey, currentDate.Format(apodDateFormat))
		go c.getPicture(&wg, url, resChan, errChan)

		currentDate = currentDate.AddDate(0, 0, 1)
	}

	wg.Wait()
	close(resChan)
	close(errChan)

	if err := <-errChan; err != nil {
		return domain.Pictures{}, fmt.Errorf("cannot get pictures URLs from NASA APOD API: %w", err)
	}

	for result := range resChan {
		pictures.URLs = append(pictures.URLs, result.URL)
	}

	return pictures, nil
}

func (c *PictureService) getPicture(wg *sync.WaitGroup, url string, resChan chan<- pictureResponse, errChan chan<- error) {
	defer func() {
		wg.Done()
		<-c.runningGoroutines
	}()
	resp, err := c.client.Get(url)
	if err != nil {
		errChan <- fmt.Errorf("%w: error on sending GET request: %s", domain.ErrInternalServerError, err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		errMessage := fmt.Sprintf("expected status 200, got %s", resp.Status)

		errResponse := errResponse{}
		err = json.NewDecoder(resp.Body).Decode(&errResponse)
		if err != nil {
			log.Warn(fmt.Errorf("error on decoding eror response from NASA APOD API: %s", err))
		}

		if errResponse.Message != "" {
			errMessage = fmt.Sprintf("%s, returned error message: '%s'", errMessage, errResponse.Message)
		}

		errChan <- fmt.Errorf("%w: %s", domain.ErrExternalAPIError, errMessage)
		return
	}

	pictureResp := pictureResponse{}

	err = json.NewDecoder(resp.Body).Decode(&pictureResp)
	if err != nil {
		errChan <- fmt.Errorf("%w: error on decoding response: %s", domain.ErrInternalServerError, err)
		return
	}

	resChan <- pictureResp
}
