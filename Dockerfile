FROM golang:1.16.5-alpine3.13 as builder
RUN mkdir /build
COPY . /build/
WORKDIR /build
RUN go build -o url-collector .

FROM alpine:3.14.0

COPY --from=builder /build/url-collector /app/
WORKDIR /app

ARG CONCURRENT_REQUESTS
ARG API_KEY
ARG PORT

ENV API_KEY=$API_KEY
ENV PORT=$PORT
ENV CONCURRENT_REQUESTS=$CONCURRENT_REQUESTS

ENTRYPOINT ["./url-collector"]
