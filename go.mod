module gitlab.com/shestel12/U3RhbmlzbGF1IEdvZ29BcHBzIE5BU0E

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20190624142023-c5567b49c5d0 // indirect
)
